# Accelerometers Evaluation Board



## Project brief
I needed to test some accelerometers for another project but couldn't find evaluation boards, so I made one.
The board is designed to work with the MMA8653FC and the LIS2DW12TR chips at the same time but of course you can use one or the other.

## Name
MMA8653FC/LIS2DW12TR evaluation board
